

function createNewUser(){
  let firstName = prompt('Choose your firstName', '')
  while (firstName === '') {
    firstName = prompt('Choose your firstName again', '')
  };
  let secondName = prompt('Choose your secondName', '')
  while (secondName === '') {
    secondName = prompt('Choose your secondName again', '')
  };
  let birthday = prompt('Choose your birthday date dd.mm.yyyy)', '')
    while (birthday === '') {
    birthday = prompt('Choose your birthday date again dd.mm.yyyy)', '')
  };
  let newUser = {
    firstName,
    secondName,
    birthday,
    getLogin () {
      return console.log((this.firstName.charAt(0) + this.secondName).toLowerCase());
    },
    getAge(){
      let now = new Date();
      let currYear = now.getFullYear();
      let userDate = +this.birthday.slice(0, 2);
      let userMonth = +this.birthday.slice(3, 5);
      let userYear = +this.birthday.slice(6, 10);
      let birthDate = new Date(userYear, userMonth-1, userDate);
      let birthYear = birthDate.getFullYear();
      let age = currYear - birthYear;
      if (now < new Date(birthDate.setFullYear(currYear))) {
        age = age - 1;
      }
      return console.log(age);
    },
    getPassword(){
      return console.log(this.firstName.charAt(0).toUpperCase() + this.secondName.toLowerCase() + this.birthday.slice(6, 10)); 
    },
  }
  newUser.getLogin();
  newUser.getAge();
  newUser.getPassword();
  return newUser

}

console.log(createNewUser());
